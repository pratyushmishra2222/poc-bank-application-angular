import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { accounts } from './accounts';
import { ProjectServiceService } from './project-service.service';
import { AccountWithProjectId } from './AccountWithProjectId';


@Injectable({
  providedIn: 'root'
})
export class AccountServiceService {

  constructor(private httpclient : HttpClient,private projectService : ProjectServiceService) { }
  url = 'http://localhost:3000/accounts';
  urlM = 'http://localhost:3000/accounts?';

  getAccounts(data:number[]){
        
    let idParam = '';
    data.forEach(function (value: any) {
      idParam = idParam + 'id' + '=' + value+'&';
    });
    idParam=idParam.slice(0, -1); 
    let linka = this.urlM + idParam;
    return this.httpclient.get(linka) as Observable<accounts>;
     
  }
  
  getAccountWithId(id:number){
    return this.httpclient.get(this.url+'/'+id) as Observable<accounts>;
  }
  addAccounts(body : accounts,ID : number){
    
    let item : AccountWithProjectId = {
      body:body,
      ProjectId : Number(ID)
    }
    
    // const headers = new HttpHeaders()
    // .set('content-type', 'application/json')
    // .set('Access-Control-Allow-Origin', '*');
    console.log(item);
    
    return this.httpclient.post(this.url,item) /*as Observable<accounts>;*/ // changed here
  }
  deleteAccounts(id : number){
    return this.httpclient.delete(this.url+'/'+id) as Observable<accounts>;
  }
  
  updateAccounts(id:number,body:accounts){
    return this.httpclient.put(this.url+'/'+id,body) as Observable<accounts>;
  }
  getAllAccounts(){
      return this.httpclient.get(this.url+"/all") as Observable<accounts>;
  }
  


  addNewUserIdsToTheAccountId(accid : number,userid:number) : Observable<accounts>{
    let result : number[] = [];
    
    this.httpclient.get(this.url+'/'+accid).subscribe((accounts:any)=>{
      
    result = [...accounts.userIds];
    result.push(userid);
    accounts.userIds = result;

    this.httpclient.put(this.url+'/'+accid,accounts);
    })
    return this.httpclient.get(this.url+'/'+accid) as Observable<accounts>;
  }
  
  
}
