import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit 
{
  @Input() param1  = '';
  @Input() param2  = '';
  @Input() param3  = '';
  
  constructor() {}
  ngOnInit(): void {}

}
