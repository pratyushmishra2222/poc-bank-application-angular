import { Injectable } from '@angular/core';
export interface project{
    id : number
    name : string 
    owner :string
    des : string
    accountIds : number[]
}

@Injectable()
export class project {
    id : number = -1;
    name : string = '';
    owner :string = '';
    des : string= '';
    accountIds : number[] = [];

}