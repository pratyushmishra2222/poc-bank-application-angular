import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { project } from './project';

@Injectable({
  providedIn: 'root'
})
export class ProjectServiceService {

  constructor(private httpclient : HttpClient,private activated : ActivatedRoute) { }
  url = 'http://localhost:3000/projects';
  countries = 'https://restcountries.eu/rest/v2/all';
  // urlM = 'http://localhost:3000/projects?';

  getAllProjects(){
    return this.httpclient.get(this.url) as Observable<project>;
  }
  getProjectWithId(id:number)
  {
    return this.httpclient.get(this.url+'/'+id) as Observable<project>;
  }
  addProject(body : project){
    return this.httpclient.post(this.url,body) as Observable<project>;
  }
  deleteProject(id : number){
    return this.httpclient.delete(this.url+'/'+id) as Observable<project>;
  }
  updateProject(id:number,body:project){
    return this.httpclient.put(this.url+'/'+id,body) as Observable<project>;
  }
  getCountriesName(){
    return this.httpclient.get(this.countries);
  }
  addNewAccountsToTheProjectId(prid : number,accid:number)
  {
    let result : number[] = [];
    
    this.getProjectWithId(prid).subscribe((project:project)=>{
      
    result = [...project.accountIds];
    result.push(accid);
    project.accountIds = result;
    
    this.updateProject(prid,project);
    })
    return this.getProjectWithId(prid) as Observable<project>;
  }
  
  removeaccountIdFromProject(prid : number,accid:number) : Observable<project>{
    let result : number[] = [];
    
    this.getProjectWithId(prid).subscribe((project:project)=>
    {
    result = [...project.accountIds];
    result.forEach((element:number,index:number)=>{
      if(element === accid) result.splice(index, 1);
    })
    project.accountIds = result;
    this.httpclient.put(this.url+'/'+prid,project);
    })
    return this.getProjectWithId(prid) as Observable<project>;
  }

  getLength() : number{
    let res = 0;
    this.getAllProjects().subscribe((val:project)=>{
      res = Object.keys(val).length;
    })
    return res;
  }
}
