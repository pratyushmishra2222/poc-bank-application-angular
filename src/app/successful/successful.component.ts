import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { project } from '../project';
import { ProjectServiceService } from '../project-service.service';
import { helpme } from '../projects/helpme';

@Component({
  selector: 'app-successful',
  templateUrl: './successful.component.html',
  styleUrls: ['./successful.component.css']
})
export class SuccessfulComponent implements OnInit {
  obser: Observable<any> | undefined;
  val : any;
  list1 : any;
  
  constructor(private service : ProjectServiceService,private router: Router,public helpme:helpme) {}
  ngOnInit(): void {
    this.helpme.setTitle('successful page')
    this.send();
  }
  project(){
    this.router.navigateByUrl('successful/projects')
  }
  send(){
    this.obser =  this.service.getAllProjects();
    this.obser.subscribe((val:project) => {
     this.list1 = val;
    })
  }
  
  
}
