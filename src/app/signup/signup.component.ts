import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { register } from '../forms-basic/register';
import { LoginService } from '../login.service';
import {Location} from '@angular/common';
import { helpme } from '../projects/helpme';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private ngxspinner:NgxSpinnerService,private location:Location,public helpme:helpme,private loginservice : LoginService,private cookieService: CookieService,public router:Router,private SpinnerService : NgxSpinnerService, ) { }

  ngOnInit(): void {
  }
  created : boolean = false;
  addUser(signupForm : NgForm)
  {
    
    let add : register = {
      email : signupForm.value.Email,
      firstName : signupForm.value.firstName,
      lastName : signupForm.value.lastName,
      password : signupForm.value.Password,
      token : ''
    }
    this.ngxspinner.show();
    this.loginservice.signup(add).subscribe(
      () =>{
        this.ngxspinner.hide()
        this.cookieService.set('isSignedUp' , 'true');
        this.location.back();
       
        },
        err => console.log(err)
    )
  }

}
