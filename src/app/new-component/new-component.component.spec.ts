// import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
// import { By } from 'protractor';


import { NewComponentComponent } from './new-component.component';

// let val : NewComponentComponent;
describe('NewComponentComponent', () => 
{
  let component: NewComponentComponent;
  let fixture: ComponentFixture<NewComponentComponent>; //doubt
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  fit('should have the title "hello indians"',()=>{
    expect(component.title).toBe('hello indians')
  });

  //it will check if the content of h1 has 'hello' in it or not?
  it('content of h1 contains "hello"',()=>{
    expect(component.content).toContain('hello');
  });

  //exact match of a string
  it('content of h1 contains "hello world"',()=>{
    expect(component.content).toBe('hello world');
  });

  
  //how to test for functions
  it('addition function',() =>{
    expect(component.addition(10,20)).toBe(30);
  });
  

  //string testing
  //toBe is used for string
  it('string matching',()=> {
    const variable = 'india';
    expect(variable).toBe('india');
    expect(variable).not.toBe('chennai');
  });

  //toMatch is used for regular expression concept
  it('"toMatch" demonstarion',()=>{
    const stringval = "this is my 45 contrueydfd";
    expect(stringval).toMatch("\\d+");
    expect(stringval).toMatch("this");
  });


  //arrays checking
  //toequal and toContain

  it('array testing',()=>{
    let arr = [1,2,3];
    expect(arr).toEqual([1,2,3]);
    expect(arr.length).not.toEqual(6);
  });

  fit('string array checking',()=>{
    let str = ['hello','1234','fine'];
    expect(str[1]).toContain("2");
  });





});
