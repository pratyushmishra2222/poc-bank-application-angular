import { Component, EventEmitter, Input, OnInit, Output,ChangeDetectorRef } from '@angular/core';
import {helpme} from '../projects/helpme';
@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.css']
})
export class TableComponentComponent implements OnInit {
  @Input() dataList: any[] = []; 
 
  @Output() idClicked = new EventEmitter<any>(); 

  constructor(public helpme:helpme,private cdref: ChangeDetectorRef) {}
  
  ngOnInit(): void {}
  ngAfterContentChecked() {this.cdref.detectChanges();}
  onIdClicked(index : number) 
  {
    this.idClicked.emit(this.dataList[index]);
  }
  
  
}
