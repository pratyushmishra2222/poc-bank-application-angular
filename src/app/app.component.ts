import { transition, trigger, useAnimation } from '@angular/animations';
import { Component } from '@angular/core';
import { moveFromRight } from 'ngx-router-animations';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    
    trigger('moveFromRight', [
      transition('* => *', useAnimation(moveFromRight))
    ])
  ]
})
export class AppComponent {
  title = 'angular-project';
  public getState(outlet: any) {
    return outlet.activatedRouteData.state;
  }
}
