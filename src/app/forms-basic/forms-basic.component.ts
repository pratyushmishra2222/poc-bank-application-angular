import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Location } from "@angular/common";
import { LoginService } from '../login.service';
import { helpme } from '../projects/helpme';
import { login } from './login';

import { ErrorMessage } from './ErrorMessage';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-forms-basic',
  templateUrl: './forms-basic.component.html',
  styleUrls: ['./forms-basic.component.css']
})
export class FormsBasicComponent implements OnInit {

  formStatus = true;
  isSignedUp = this.cookieService.get('isSignedUp');
  emailValue: boolean = true;
  passwordValue: boolean = true;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  isValidFormSubmitted = false;
  constructor(private ngxSpinner: NgxSpinnerService,
    public cookieService: CookieService,
    private router: Router,
    public helpme: helpme,
    public loginservice: LoginService,
    public ErrorMessage: ErrorMessage) { }
  

  validate(contactForm: NgForm) {


    if (contactForm.value.email.length === 0 || contactForm.value.password.length == 0) {
      this.router.navigateByUrl('not-allowed');
    }

    let obj: login = {
      email: contactForm.value.email,
      password: contactForm.value.password
    }
this.ngxSpinner.show();
    this.loginservice.login(obj).subscribe((item: ErrorMessage) => {
      localStorage.setItem('token',item._Token);
      this.ngxSpinner.hide();
      if (item._isCorrect) {
        this.formStatus = true;
        this.cookieService.set('shouldAllow', 'true');
        console.log(item._Token);
        
        this.router.navigateByUrl('forms/projects');
      }
      else if (!item._isCorrect) {
        if (item._WhatIsWrong == "email") {
          this.emailValue = false;
          contactForm.controls['email'].reset()
          console.log(item._Token);
          this.formStatus = true;
        }
        else if (item._WhatIsWrong == "password") {
          this.passwordValue = false;
          contactForm.controls['password'].reset()
          console.log(item._Token);
          this.formStatus = true;
        }
        else if (item._WhatIsWrong == "both") {
          console.log(item._Token);
          contactForm.reset();
          this.formStatus = false;
          this.emailValue = true;
          this.passwordValue = true;
        }
      }

    }
    )
  }

  ngOnInit(): void {
    this.helpme.setTitle('LOGIN PAGE')
    this.cookieService.deleteAll();
  }


}
