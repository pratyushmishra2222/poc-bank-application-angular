
import { Injectable } from '@angular/core';
export interface ErrorMessage{
    
    _isCorrect : boolean
    _WhatIsWrong : string
    _Token : string
    
}

@Injectable()
export class ErrorMessage{
    
    _isCorrect : boolean=false
    _WhatIsWrong : string=''
    _Token : string=''

    
}