import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-allowed-page',
  templateUrl: './not-allowed-page.component.html',
  styleUrls: ['./not-allowed-page.component.css']
})
export class NotAllowedPageComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }
  GoTOLogInPage(){
    this.router.navigate(['forms/login'])
  }
}
