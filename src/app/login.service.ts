import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ErrorMessage } from './forms-basic/ErrorMessage';
import { login } from './forms-basic/login';
import { register } from './forms-basic/register';

// import { RequestOptions} from ''
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpclient : HttpClient,private ErrorMessage : ErrorMessage) { }

    urlLogin : string = 'http://localhost:3000/Login'; // [controller]
    urlSignup : string = 'http://localhost:3000/SignUp'; // [controller]
    
    login(login : login){
     
      return this.httpclient.post(this.urlLogin+'/userLogin',login) as Observable<ErrorMessage>;
    }
    
    signup(register : register){
      // let httpheaders = new HttpHeaders({
      //   'content-type' : 'application-json'
      // })
      // httpheaders = httpheaders.set('token',);
      return this.httpclient.post<register>(this.urlSignup+'/createcontact',register) as Observable<register>;
    }
}
