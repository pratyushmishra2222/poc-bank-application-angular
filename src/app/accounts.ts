
import { Injectable } from '@angular/core';

export interface accounts{
    name : string,
    id : number,
    country : string,
    about : string ,
    userIds : number[]
}

@Injectable()
export class accounts{
    
    name : string = '';
    id : number = -1;
    country : string = '';
    about : string = '';
    userIds : number[] = [];

    
}