import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewComponentComponent } from './new-component/new-component.component';
import { FormsBasicComponent } from './forms-basic/forms-basic.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuccessfulComponent } from './successful/successful.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from "ngx-spinner"; 
import { HttpClientModule} from '@angular/common/http';
import { TableComponentComponent } from './table-component/table-component.component';
import { ProjectsComponent } from './projects/projects.component';
import { AccountsComponent } from './accounts/accounts.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { UsersComponent } from './users/users.component';
import { DetailsComponent } from './details/details.component';
import { ProjectDetailsComponent } from './projects/project-details/project-details.component';
import { AccountDetailsComponent } from './accounts/account-details/account-details.component';
import { UsersDetailsComponent } from './users/users-details/users-details.component';
import { helpme } from './projects/helpme';
import { users } from './users';
import { accounts } from './accounts';
import { KeysPipe } from './keys.pipe';
import { RewardComponent } from './reward/reward.component';
import { LogoutComponent } from './logout/logout.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { register } from './forms-basic/register';
import {  SignupComponent } from './signup/signup.component';
import { CookieService } from 'ngx-cookie-service';
import { NotAllowedPageComponent } from './not-allowed-page/not-allowed-page.component';
import { AuthGuard } from './auth.guard';
import { ErrorMessage } from './forms-basic/ErrorMessage';


@NgModule({
  declarations: [
    AppComponent,
    NewComponentComponent,
    FormsBasicComponent,
    SuccessfulComponent,
    TableComponentComponent,
    ProjectsComponent,
    AccountsComponent,
    ErrorPageComponent,
    UsersComponent,
    DetailsComponent,
    ProjectDetailsComponent,
    AccountDetailsComponent,
    UsersDetailsComponent,
    KeysPipe,
    RewardComponent,
    LogoutComponent,
    SpinnerComponent,
    SignupComponent,
    NotAllowedPageComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxSpinnerModule , 
    BrowserAnimationsModule
  ],
  providers: [helpme,users,accounts,register,CookieService,AuthGuard,ErrorMessage],
  bootstrap: [AppComponent]
})
export class AppModule { }
