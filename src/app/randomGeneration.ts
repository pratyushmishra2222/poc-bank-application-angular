import { Injectable } from "@angular/core";

@Injectable()
export class randomGeneration{
    getRandomBankName(): string {
        var text = "";
        var options = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  
        for (var i = 0; i < 4; i++)
          text += options.charAt(Math.floor(Math.random() * options.length));
  
        return text;
      }
      getRandomAmount(): string {
        var text = "";
        var options = "123456789";
  
        for (var i = 0; i < 5; i++)
          text += options.charAt(Math.floor(Math.random() * options.length));
  
        return text;
      }
      getRandomAccno(): number {
        var text = "";
        var options = "123456789";
  
        for (var i = 0; i < 5; i++)
          text += options.charAt(Math.floor(Math.random() * options.length));
  
        return Number(text);
      }
}