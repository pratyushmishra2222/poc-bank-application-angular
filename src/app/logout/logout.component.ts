import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private location:Location,private cookieService : CookieService) { }

  ngOnInit(): void {
  }
  back(){
    this.location.back();
  }
  resetCookieStatus(){
    this.cookieService.set('shouldAllow', 'false');
  }
  
}
