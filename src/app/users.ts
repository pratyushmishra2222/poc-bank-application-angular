import { Injectable } from '@angular/core';

export interface users{
  id : number,
  name : string,
  bank : string,
  accNo : number,
  balance : number
}

@Injectable()
export class users{
 
  id : number = -1;
  name : string = '';
  bank : string = '';
  accNo : number = 24124123123;
  balance : number = 2000
}
