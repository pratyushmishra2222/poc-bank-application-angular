import { HttpClient } from '@angular/common/http';
import {  Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {  Observable} from 'rxjs';





@Injectable(
  {
  providedIn: 'root'
  }
)
export class ListService {
    
  
      projects : Observable<any> | undefined;
      constructor(private httpclient : HttpClient,private activated : ActivatedRoute) {}
      
      //projects
      getListByURL(){
      let linka = 'http://localhost:3000/projects';
      return this.httpclient.get(linka);
        
      }
     
      

      addProject(body : any){
        return this.httpclient.post('http://localhost:3000/projects',body);
      }


      
      getListByURLWithId(id:string){
        let linka = 'http://localhost:3000/projects?id='+id;
        return this.httpclient.get(linka);
      }
      getListByURLByIdProjects(id : any){
        let linka = 'http://localhost:3000/projects?id='+id;
        return this.httpclient.get(linka);
          
      }
      
      getListByURLArray(datas : []){
        let idParam = '';
      
        datas.forEach(value =>{
          idParam = idParam + 'id' + '=' + value+'&';
        })
        let linka = 'http://localhost:3000/projects?' + idParam;
        return this.httpclient.get(linka);
      }

      //accounts
      getListByURLA(){
      let linka = 'http://localhost:3000/accounts';
      return this.httpclient.get(linka);
        
      }
      getAccountsById(id : any){
        let linka = 'http://localhost:3000/accounts?id='+id;
        return this.httpclient.get(linka);
          
      }

      updateAccount(id :any,update:any){
        let newUrl =  'http://localhost:3000/accounts?id='+id;
        
        return this.httpclient.put(newUrl,update);
      }

      deleteAccount(id : any){
        let newUrl =  'http://localhost:3000/accounts?id='+id;
        return this.httpclient.delete(newUrl);
      }

      getListByURLAId(accountIds: any){
        
        let idParam = '';
        accountIds.forEach(function (value:any) {
          console.log(value);
          
          idParam = idParam + 'id' + '=' + value+'&';
          // console.log(idParam);
        });
      
        let linka = 'http://localhost:3000/accounts?' + idParam;
        return this.httpclient.get(linka);
          
      }
      // getUsersLength() : number{

      // }
     
      getusers(data: any){
        
        let idParam = '';
        data.forEach(function (value: string) {
          // console.log(value);
          
          idParam = idParam + 'id' + '=' + value+'&';
          // console.log(idParam);
        });
      
        let linka = 'http://localhost:3000/users?' + idParam;
        return this.httpclient.get(linka);
         
      }
      //add
      addUser(body : any){
        return this.httpclient.post('http://localhost:3000/users',body);
      }

      accountAdd(body : any){
        return this.httpclient.post('http://localhost:3000/accounts',body);
      }
      getAllusers(){
        return this.httpclient.get('http://localhost:3000/users');
      }
      
      addUserToAccount(userId:number, id:number) {
         this.httpclient.get(`http://localhost:3004/accounts/${id}`).subscribe((account:any) => {
          account.userIds.push(userId);
          this.httpclient.put(`http://localhost:3004/account/${id}`,account);
        })
        return this.httpclient.get('http://localhost:3004/accounts');
      }
      
 }
    

