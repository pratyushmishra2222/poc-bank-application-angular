import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { users } from './users';

import { AccountServiceService } from './account-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private httpclient : HttpClient) { }
  url : string = 'http://localhost:3000/users';
  urlM : string = 'http://localhost:3000/users?';
  
  getusers(data: number[])
  {
        
    let idParam = '';
    data.forEach(function (value: any) {
      idParam = idParam + 'id' + '=' + value+'&';
    });
    idParam=idParam.slice(0, -1); 
    let linka = this.urlM + idParam;
    return this.httpclient.get(linka);
     
  }
  getNoOfUsers(){
    this.getAllusers().subscribe(items=>{
      Object.keys(items).length

    })
  }
  getUser(id:number) : Observable<users>{
    return this.httpclient.get(this.url+'/'+id) as Observable<users>;
  }
  addUser(body : users){
    return this.httpclient.post(this.url,body) as Observable<users>;
  }
  deleteUser(id : number){
    return this.httpclient.delete(this.url+'/'+id) as Observable<users>;
  }
  updateUser(id:number,body:users){
    return this.httpclient.put(this.url+'/'+id,body) as Observable<users>;
  }
  getAllusers(){
      return this.httpclient.get(this.url+"/all") as Observable<users>;
  }

  
}
