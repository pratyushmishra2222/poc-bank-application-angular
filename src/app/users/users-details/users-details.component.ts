import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { UserServiceService } from 'src/app/user-service.service';
import { users } from 'src/app/users';

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.css']
})
export class UsersDetailsComponent implements OnInit {

  obser: Observable<any> | undefined;
  bank : string = '';
  userId:number=-1;
  ngOnInit(): void {}
  constructor(private SpinnerService : NgxSpinnerService,private activate : ActivatedRoute,private service : UserServiceService,private users:users) { 
    this.SpinnerService.show();
    this.activate.params.subscribe(data=>{
     this.userId=data.id2;
     this.fetchTheBankName(this.userId);
     this.SpinnerService.hide();
    })
  }
  fetchTheBankName(id:number){
    console.log('inside fetch the bank');
    this.service.getUser(id).subscribe((val:users)=>{
      this.bank = val.bank;
    })
    
    
  }

}
