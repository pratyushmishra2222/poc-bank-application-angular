import { Component, Input, OnInit} from '@angular/core';
import { Observable } from 'rxjs';
import {Location} from '@angular/common';
import { accounts } from '../accounts';
import { UserServiceService } from '../user-service.service';
import { AccountServiceService } from '../account-service.service';
import { helpme } from '../projects/helpme';
import { users } from '../users';
import { Router } from '@angular/router';
import { randomGeneration } from '../randomGeneration';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  obser: Observable<any> | undefined;
  list1 : any;
  
  @Input() ID : any; //id of the account
 
  userIds : number[] = [];
  bankName : string=""
  accNos : number = 0;
  
  balance : number = 0;
  type : string="users";
  deletedId : string = '';
  constructor(private router:Router,public helpme:helpme,private accountservice: AccountServiceService,private userservice:UserServiceService,private Location: Location) {}
 
  ngOnInit(): void {
    this.helpme.setTitle('users page')
    this.obser = this.userservice.getAllusers();
    this.obser.subscribe((data)=>{
     
    this.user.id  = data[data.length-1].id+1;
    })
    
    this.accountservice.getAccountWithId(this.ID).subscribe(val=>{
     this.userIds = Object.values(val)[4];
    this.list1 = this.getUsersWithUserIds(Object.values(val)[4]);
    });
  }
  back(){this.Location.back();}
  getUsersWithUserIds(userIds : number[])
  {
    this.obser =  this.userservice.getusers(userIds);
    this.obser.subscribe(val => {
     this.list1=val;
    })
  }
  user : users = {
    id: -1,
    name: '',
    bank:new randomGeneration().getRandomBankName(),
    accNo:new randomGeneration().getRandomAccno(),
    balance:0
  }
  msg : boolean = false;
  onAdd(){this.addUser();}


  /**
   * 
   * addUser(){
    let result : number[] = [];
    this.obser = this.userservice.addUser(this.user)
    this.obser.subscribe(val=>
      {
      result = [...this.userIds]; 
      result.push(Number(this.user.id)); 
      this.accountservice.addNewUserIdsToTheAccountId(this.ID,this.user.id).subscribe((val:accounts)=>{
        val.userIds.push(this.user.id)
        this.accountservice.updateAccounts(this.ID,val).subscribe(res =>{
          console.log(res);
        })
        console.log(val);
      })
      this.getUsersWithUserIds(result);
    })
    this.msg=true;
  }

   */
  addUser(){
    let result : number[] = [];
    this.obser = this.userservice.addUser(this.user)
    this.obser.subscribe((val:users)=>
    {
      result = [...this.userIds]; 
      result.push(Number(this.user.id))
      
      this.accountservice.getAccountWithId(this.ID).subscribe((val:accounts)=>{
        
        val.userIds.push(this.user.id)
        let value : accounts = {
          name:val.name,
          id : val.id,
          country : val.country,
          about : val.about,
          userIds : val.userIds
        }
        
        this.accountservice.updateAccounts(this.ID,value).subscribe(res =>{})
        
        this.getUsersWithUserIds(val.userIds)
      })

    })
    this.msg=true;
  }
  onDelete(){this.deleteUser(Number(this.deletedId));}




  /**
   * 
   * deleteAccount(id:number)
  {
    
    this.service.deleteAccounts(id).subscribe();
    this.projectservice.getProjectWithId(this.ID).subscribe((val:project)=>
    {
      val.accountIds = val.accountIds.filter(item=>item!=id);
      this.projectservice.updateProject(this.ID,val).subscribe()
      this.getAccountsWithAccountIds(val.accountIds)
    })
    //this.removeFromTheProjectsHavingThisAccountId(id)
    this.msg=true;
    // this.projectservice.getProjectWithId(this.ID).subscribe((project:project)=>{
    //   this.getAccountsWithAccountIds(project.accountIds);
    // });

    //this.removeFromTheProjectsHavingThisAccountId(id); doing in the backend!
  }
   */
  deleteUser(id:number)
  {
    
    this.accountservice.deleteAccounts(id).subscribe();
    
    this.accountservice.getAllAccounts().subscribe((val:any)=>{
      val.forEach((element:accounts) => {
        if(element.userIds.includes(id)){
          element.userIds = element.userIds.filter(item=>item!=id);
          this.accountservice.updateAccounts(this.ID,element).subscribe()
        }
      });
    })



    this.accountservice.getAccountWithId(this.ID).subscribe((val:accounts)=>
    {
      // val.accountIds = val.accountIds.filter(item=>item!=id);
      // this.projectservice.updateProject(this.ID,val).subscribe()
      this.getUsersWithUserIds(val.userIds)
    })
    //this.removeFromTheProjectsHavingThisAccountId(id)
    this.msg=true;
  }
  // deleteUser(id:number)
  // {
  //   this.userservice.deleteUser(id).subscribe((val:users)=>{});
  //   this.accountservice.getAccountWithId(this.ID).subscribe((val:accounts)=>
  //   {
  //     val.userIds = val.userIds.filter(item=>item!=id);
  //     this.accountservice.updateAccounts(this.ID,val).subscribe()
  //     this.getUsersWithUserIds(val.userIds)
  //   })
  //   //this.removeFromTheAccountsHavingThisUserId(id);
  // }
  
  removeFromTheAccountsHavingThisUserId(userid : number)
  {
    
      this.accountservice.getAllAccounts().subscribe((val:any)=>
      {
          val.forEach((element:accounts) => 
          {
              if(element.userIds.includes(userid))
              {
                element.userIds = element.userIds.filter(item=>item!=userid)
                //this.accountservice.updateAccounts(element.id,element).subscribe((acc : accounts)=>{})
              }
              
              //this.getUsersWithUserIds(val[element.id-1].userIds);
          });
        
      })
      this.accountservice.getAccountWithId(this.ID).subscribe((val:accounts)=>{
        this.getUsersWithUserIds(val.userIds)
      })
      this.msg=true;
  }
  updatedUser : users={
    id: -1,
    name: '',
    bank:'',
    accNo:0,
    balance:0
  }
  onChange(e:any){
    console.log(e.target.value);
    this.userservice.getUser(e.target.value).subscribe((val:users)=>{
      this.updatedUser.name = val.name;
      this.updatedUser.bank=val.bank;
      this.updatedUser.accNo=val.accNo;
      this.updatedUser.balance=val.balance;
    })
  }
  reload(){
    
    window.location.reload()
  }
  updatedId:number = 0;
  onUpdate(id:number){
   this.updateUser(id)
   window.location.reload(); 
  }
 
  getBankName(id:number){
    this.userservice.getUser(id).subscribe((val:users)=>{
      let user1 : users = 
      {
        id : val.id,
        name : this.user.name,
        bank:val.bank,
        accNo : val.accNo,
        balance : val.balance
      };
      this.userservice.updateUser(id,user1).subscribe(val=>{
        this.getUsersWithUserIds(this.userIds);
      })
    })
  }
  updateUser(update:number){
    console.log(`update user clicked ${update}`);
    this.getBankName(update);
    
    this.msg=true;
  }
  showTheUserClicked(data : any){
    console.log('inside showTheUserClicked');
    this.router.navigate([this.router.url+'/users/'+data.id]);
  }
  variable : string="" ;
  userType : string = ""; // you have to make this a new column in users table by the name user type.change in json too.
  color:string = ""
 
  typeOfUserAdded(e:any){
    this.variable = this.helpme.getTheUserType(Number(e.target.value))[0];
    this.userType = this.helpme.getTheUserType(Number(e.target.value))[1];
    this.color = this.helpme.getTheUserType(Number(e.target.value))[2];
  }
}


