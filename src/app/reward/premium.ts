import { Injectable } from "@angular/core";

export interface premium{
    
    rewardPoints : number
    vouchers : string[]
    promotions : string[],
    color : string,
    icon : string
}

@Injectable()
export class premium {
    rewardPoints : number = 25
    vouchers : string[] = 
    [
        '12% off on all purchases from azamon on electronics',
        '15% off on all purchases from filpkrat on dailycare',
        '17% off on all purchases from gib zabaar',
        '20% off on all purchases using debit/Credit card'
    ]

    promotions : string[] = 
    [
        'Earn upto ₹2,00,000,Start investing Now',
        'Secure your future,Get a Life insurance Now',
        'Gold Loan @₹1000/mo',
        'Car Loan @10% interest',
        'Education Loan @7%  interest'
    ]
    color : string = '#7b1fa2'
    icon : string = 'fab fa-buysellads'
}