import { Component, Input, OnInit } from '@angular/core';

import { UserServiceService } from '../user-service.service';

import { helpme } from 'src/app/projects/helpme';
import { users } from '../users';
import {Location} from '@angular/common';
import { time } from '../getTime';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-reward',
  templateUrl: './reward.component.html',
  styleUrls: ['./reward.component.css']
})
export class RewardComponent implements OnInit {

  constructor(public titleservice:Title,public helpme:helpme,private service : UserServiceService,private users:users,private Location : Location) { }
  
  balance:number=0;
  @Input() bank:string='';
  getDate : string = '';
  userName:string='';
    
  @Input() userId : number = -1;
  
  ngOnInit(): void {
    this.fetchTheDetails(this.userId);
    this.getDate=new time().getTheDate();
    this.helpme.setTitle('rewards page')
  }
  
  fetchTheDetails(userId: number) {
    
    this.service.getUser(userId).subscribe((val:users)=>{
    this.userName=val.name;
    this.balance=val.balance;
    this.bank=val.bank;
    this.helpme.getTheUserType(this.balance);
    })
 }
  

}
