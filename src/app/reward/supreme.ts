import { Injectable } from "@angular/core";

export interface supreme{
    rewardPoints : number
    vouchers : string[]
    promotions : string[],
    color : string,
    icon : string
}

@Injectable()
export class supreme {
    
    rewardPoints : number = 55
    vouchers : string[] = [
        '32% off on all purchases from azamon on electronics',
        '35% off on all purchases from filpkrat on dailycare',
        '47% off on all purchases from gib zabaar',
        '60% off on all purchases using debit/Credit card'
    ]

    promotions : string[] = [
        'Earn upto ₹5,00,000,Start investing Now',
        'Secure your future,Get a Life insurance Now',
        'Gold Loan @₹300/mo',
        'Car Loan @4% interest',
        'Education Loan @7%  interest'
    ]
    color : string = '#26a69a'
    icon : string = 'fab fa-centos'
}