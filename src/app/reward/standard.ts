import { Injectable } from "@angular/core";

export interface standard{
    
    rewardPoints : number
    vouchers : string[]
    promotions : string[],
    color : string,
    icon : string
}

@Injectable()
export class standard implements standard{
    
   
    
    rewardPoints : number = 10;

    vouchers : string[] = [
        '2% off on all purchases from azamon on electronics',
        '5% off on all purchases from filpkrat on dailycare',
        '7% off on all purchases from gib zabaar',
        '10% off on all purchases using debit/Credit card'
    ]

    promotions : string[] = [
        'Earn upto ₹1,00,000,Start investing Now',
        'Secure your future,Get a Life insurance Now',
        'Gold Loan @₹2000/mo',
        'Car Loan @12% interest',
        'Education Loan @7%  interest'
    ]
    color : string = '#3f51b5'
    icon : string = 'fab fa-asymmetrik'
    
}