import { Injectable } from "@angular/core";

export interface ultra{
    
    rewardPoints : number
    vouchers : string[]
    promotions : string[],
    color : string,
    icon : string
}

@Injectable()
export class ultra {
    
    rewardPoints : number = 35
    vouchers : string[] = [
        '22% off on all purchases from azamon on electronics',
        '28% off on all purchases from filpkrat on dailycare',
        '37% off on all purchases from gib zabaar',
        '40% off on all purchases using debit/Credit card'
    ]

    promotions : string[] = [
        'Earn upto ₹3,50,000,Start investing Now',
        'Secure your future,Get a Life insurance Now',
        'Gold Loan @₹800/mo',
        'Car Loan @8% interest',
        'Education Loan @7%  interest'
    ]
    color : string = '#33691e'
    icon : string = 'fab fa-canadian-maple-leaf'
}