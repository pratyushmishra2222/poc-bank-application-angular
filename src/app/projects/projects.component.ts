import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {Location} from '@angular/common';
import { ProjectServiceService } from '../project-service.service';
import { project } from '../project';
import { helpme } from './helpme';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  
  obser: Observable<any> | undefined;
  list1 : any;
  toggle=true;
  res : any;
  msg=false;
  type="projects";
  defaultValue  : number[] = [1,2,3];
  owner : string =''
  desc : string = ''
  constructor(private SpinnerService : NgxSpinnerService,public helpme:helpme,private Location:Location,private service : ProjectServiceService,private route : Router) { }
  project : project = 
  {
    name: '',
    id: 1,
    owner:this.owner,
    des:this.desc,
    accountIds : this.defaultValue
  }
  ngOnInit(): void {
    this.helpme.setTitle('Projects Page')
    this.settingTheProjectId();
    this.send();
    // this.getNoOfProjects();
    this.toggle = !this.toggle;

  }
  settingTheProjectId(){
    this.obser = this.service.getAllProjects();
    this.obser.subscribe((data)=>{
      this.project.id  = data[data.length-1].id+1; 
    })
    this.getNoOfProjects();
  }
  onAdd(){this.addProject();}
  addProject() {
    this.obser = this.service.addProject(this.project);
    this.obser.subscribe((val:project)=>{
      this.list1 = val;
      this.send();
    });

    window.location.reload(); 
    this.msg=true;
  }
  updatedProject : project = {
    name: '',
    id: 1,
    owner:'',
    des:'',
    accountIds : []
  }
  onChange(e:any){
    console.log(e.target.value);
    this.service.getProjectWithId(e.target.value).subscribe((val:project)=>{
      this.updatedProject.name = val.name;
      this.updatedProject.owner = val.owner;
      this.updatedProject.des = val.des;
      this.updatedProject.accountIds = val.accountIds;
    })
    
  }
  onUpdate(id:number){this.updateProject(id);}
  updateProject(id:number){
    this.makeChanges(id);
    
    window.location.reload(); 
    
    this.msg=true;
  }

  reload(){
    window.location.reload()
  }
  makeChanges(id:number){
    this.service.getProjectWithId(id).subscribe((val:project)=>
    {
      let project1 : project = 
      {
        name : this.updatedProject.name,
        id : val.id, //i changed here to fix the update issue!
        owner:this.updatedProject.owner,
        des:this.updatedProject.des,
        accountIds : val.accountIds
      };
      this.service.updateProject(id,project1).subscribe(val=>{
               
        this.send();
               
      },err=>console.log(err)
      );
    });
  }
  deletedId='';
  onDelete(){this.deleteProject(Number(this.deletedId));}

  deleteProject(id : number) {
   
    this.service.deleteProject(id).subscribe((val:project)=>{
      this.msg=true;
      this.send();
      
    }, err => console.log(err));
  }
  
  send(){
    this.obser =  this.service.getAllProjects();
   
    this.SpinnerService.show();
    
      this.obser.subscribe((val:project) => {
      this.list1 = val;
     
      this.SpinnerService.hide();
    },err => console.log(err));
  }
  back(){this.Location.back();}
  navigateToProjectDetails(data : project){
    this.res = data;
    this.route.navigate(['/forms/projects/'+this.res.id]);

  }
  result : number = 1;
  resultant : number[] = [];
 
  
  getNoOfProjects() {
    this.obser = this.service.getAllProjects();
    this.obser.subscribe((data) =>{
      data.forEach((element:project) => {
        this.resultant.push(element.id)
      });
  })
 
}
}
