import { Injectable, Type } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { ProjectServiceService } from "../project-service.service";
import { premium } from "../reward/premium";
import { standard } from "../reward/standard";
import { supreme } from "../reward/supreme";
import { ultra } from "../reward/ultra";

@Injectable()
export class helpme{
    
  getHeaders(dataList : any) {
    let headers: string[] = [];
    
      if(dataList) 
      {
        dataList.forEach((value: any) => {
          Object.keys(value).forEach((key) => {
            if(!headers.find((header) => header == key)){
              headers.push(key)
            }
          })
        })
      }
    return headers;
  }
    
    constructor(private titleservice:Title,private projectservice : ProjectServiceService) {}
    
    setTitle(newTitle: string) {
      this.titleservice.setTitle(newTitle);
    }
    
    
    values : Array<string> = [];
    setValue(){
      let i=0;
      this.projectservice.getCountriesName().subscribe((data:any)=>{
         if(i++ < 15)  
         {
            data.forEach((element:any) => {
              this.values.push(element.name.substring(0,10));
            });
         }
        
      })
    }

    removeFromResult(result : number[],id:number)
    {
      let final : number[] = result.filter(word => word != id );
      return final;
    }
    
    rewardConcept : string ='Reward points are calculated on the basis of account balance and type of user'
    newMessage:string = 'You can use reward points to convert to real cash'
    color : string = '';
    vouchers : string[] = [];
    promotions : string[] = [];
    rewardPoints : number = 0;
    type : string = ''
    icon:string = '';
    
    getTheUserType(balance:number) : Array<string> {
      if(balance===0)this.type=" ";this.color="transparent";
      if(balance > 0 && balance <= 25000 ){
        this.type = 'standard';
        this.rewardPoints = new standard().rewardPoints;
        this.vouchers=new standard().vouchers;
        this.color=new standard().color;
        this.promotions = new standard().promotions;
        this.icon=new standard().icon;
      }
      else if(balance > 25000 && balance <= 50000 ){
        this.type = 'premium';
        this.rewardPoints = new premium().rewardPoints;
        this.vouchers=new premium().vouchers;
        this.color=new premium().color;
        this.promotions = new premium().promotions;
        this.icon=new premium().icon;
      }
      else if(balance > 50000 && balance <= 100000 ){
        this.type = 'ultra';
        this.rewardPoints = new ultra().rewardPoints;
        this.vouchers=new ultra().vouchers;
        this.color=new ultra().color;
        this.promotions = new ultra().promotions;
        this.icon=new ultra().icon;
      }
      else{
        this.type = 'supreme';
        this.rewardPoints = new supreme().rewardPoints;
        this.vouchers=new supreme().vouchers;
        this.color=new supreme().color;
        this.promotions = new supreme().promotions;
        this.icon=new supreme().icon;
      }
      return [this.icon,this.type,this.color];
    }
    // icon:string = "";
    
    // typeOfUser(balance:number) : Array<string> {
    //   if(balance===0)this.type=" ";this.color="transparent";
    //   if(balance > 0 && balance <= 25000 ){
    //     this.icon = new standard().icon; //move to the class
    //     this.type="ST"
    //     this.color=new standard().color;
    //   }
    //   else if(balance > 25000 && balance <= 50000 ){
    //     this.icon = new premium().icon;
    //     this.type="P"
    //     this.color=new premium().color;
    //   }
    //   else if(balance > 50000 && balance <= 100000 ){
    //     this.icon = new ultra().icon;
    //     this.type="U"
    //     this.color=new ultra().color;
    //   }
    //   else if(balance > 100000){
    //     this.icon = new supreme().icon;
    //     this.type="S"
    //     this.color=new supreme().color;
    //   }
     
    //   return [this.icon,this.type,this.color];
    
    // }
    evalue: string = "";
    emailcolors:string="";
    

    validateEmail(e:any) {
      
      let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/
      
      if (e.target.value.match(pattern)) 
      {
        this.evalue = "Your Email Address in valid"
        this.emailcolors="green"
      } else {
        
        this.evalue = "Please Enter Valid Email Address"
       this.emailcolors="red"
      }
      if (e.target.value == '') {
        
        this.evalue = ""
        this.emailcolors="transparent"
       
      }
    }
    value: string = "";
    passwordcolors : string = ""
    validatePassword(e:any) {
      
      let apattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8, 20}$/;
      var pattern = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
      console.log(e.target.value);
      
      if (e.target.value.match(pattern)) 
      {
        this.value = "Your Password in valid"
        this.passwordcolors="green"
      } else {
        
        this.value = "Please Enter Valid Password"
       this.passwordcolors="red"
      }
      if (e.target.value == '') {
        
        this.value = ""
        this.passwordcolors="transparent"
       
      }
    }
    
    
}