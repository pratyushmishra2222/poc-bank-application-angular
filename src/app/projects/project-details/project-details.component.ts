import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { project } from 'src/app/project';
import { ProjectServiceService } from 'src/app/project-service.service';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit 
{
  obser: Observable<any> | undefined;
  list1 : any;
  ID = '';
  name='';
  owner='';
  constructor(private SpinnerService : NgxSpinnerService,private ActivatedRoute : ActivatedRoute,private service : ProjectServiceService) 
  {
    this.ActivatedRoute.params.subscribe(data=>
    {
      this.ID = data.id;
      this.send();
    })
  }
  ngOnInit(): void {}
   send()
    {
      this.obser =  this.service.getProjectWithId(Number(this.ID));
      this.SpinnerService.show();
      this.obser.subscribe((val:project) => {
        this.name = val.name
        this.owner=val.owner;
        this.SpinnerService.hide();
      })
    }
}