import { Component, Input, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AccountServiceService } from '../account-service.service';
import { ProjectServiceService } from '../project-service.service';
import { project } from '../project';
import { helpme } from '../projects/helpme';
import { accounts } from '../accounts';
import { UserServiceService } from '../user-service.service';



@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
 
  obser: Observable<any> | undefined;
  list1 : any;
  @Input() ID : any;
  msg=false;
  type="accounts";
  country='';
  about='';
  accountIds : number[] = [];
  deletedId : string = '';
  constructor(public s:UserServiceService,public helpme : helpme,private projectservice : ProjectServiceService,private route : Router,private service : AccountServiceService) {}
  
 
  ngOnInit(): void 
  {
    this.settingTheAccountId();
    this.getProjectWithId();
    this.s.getNoOfUsers();
    this.helpme.setTitle('Accounts Page')
    this.helpme.setValue();
  }
  settingTheAccountId(){
    this.obser = this.service.getAllAccounts();
    this.obser.subscribe((data)=>{
      this.account.id  = data[data.length-1].id+1;
    })
  }
  
  account = 
  {
    name: '',
    id: -1,
    country:this.country,
    about:this.about,
    userIds : [2,3,4]
  }
  reload(){
    window.location.reload()
  }
  onAdd(){this.addAccount();}
  
  addAccount(){
    
    this.obser = this.service.addAccounts(this.account,this.ID) 
    this.obser.subscribe((val:accounts)=>
    {
      this.projectservice.getProjectWithId(this.ID).subscribe((val : project)=>{
               
       val.accountIds.push(this.account.id)
        let value : project = {
          name:val.name,
          id : val.id,
          owner : val.owner,
          des : val.des,
          accountIds : val.accountIds
        }
        this.projectservice.updateProject(this.ID,value).subscribe((res)=>{})

        this.getAccountsWithAccountIds(val.accountIds); 
      })
      
      // this.projectservice.addNewAccountsToTheProjectId(this.ID,this.account.id).subscribe((val : project)=>{
      //   console.log(val.accountIds + " line no 65");
        
      //   val.accountIds.push(this.account.id)
      //   this.projectservice.updateProject(this.ID,val).subscribe(res=>{console.log(val.accountIds + " line no 65")})
      //   this.getAccountsWithAccountIds(result);
      // }) 
    })
    
    this.msg=true;
    
  }
  
  onUpdate(id:number){this.updateAccount(id)}

  getUserIds(id:number){
    this.service.getAccountWithId(id).subscribe((val:accounts)=>{
      let account1 : accounts = 
      {
        name: this.updatedAccount.name,
        id: val.id,
        country:this.updatedAccount.country,
        about:this.updatedAccount.about,
        userIds : val.userIds
      };

      this.service.updateAccounts(id,account1).subscribe(val=>{
        this.getAccountsWithAccountIds(this.accountIds);
      })
    });
    
  }
  updatedAccount : accounts={
    name: '',
    id: -1,
    country:'',
    about:'',
    userIds : []
    
  }
  onChange(e:any){
    this.service.getAccountWithId(e.target.value).subscribe((val:accounts)=>{
      this.updatedAccount.name = val.name;
      this.updatedAccount.country=val.country;
      this.updatedAccount.about=val.about;
      this.updatedAccount.userIds=val.userIds;
    })
  }
  updateAccount(id:number){
   this.getUserIds(id);
   window.location.reload(); 
   this.msg=true;
  }
  
  onDelete(){this.deleteAccount(Number(this.deletedId));}
  

   deleteAccount(id:number)
  {
    
    this.service.deleteAccounts(id).subscribe();
    
    this.projectservice.getAllProjects().subscribe((val:any)=>{
      val.forEach((element:project) => {
        if(element.accountIds.includes(id)){
          element.accountIds = element.accountIds.filter(item=>item!=id);
          this.projectservice.updateProject(this.ID,element).subscribe()
        }
      });
    })



    this.projectservice.getProjectWithId(this.ID).subscribe((val:project)=>
    {
      // val.accountIds = val.accountIds.filter(item=>item!=id);
      // this.projectservice.updateProject(this.ID,val).subscribe()
      this.getAccountsWithAccountIds(val.accountIds)
    })
    //this.removeFromTheProjectsHavingThisAccountId(id)
    this.msg=true;
  }
  // removeFromTheProjectsHavingThisAccountId(accountId : number)
  // {
  //   // this.projectservice.getAllProjects().subscribe((val:any)=>
  //   // {
      
  //   //   val.forEach((element:project) => 
  //   //   {
  //   //     if(element.accountIds.includes(accountId))
  //   //     {
  //   //       element.accountIds = element.accountIds.filter(item=>item!=accountId)
  //   //     }

  //   //     //this.projectservice.updateProject(element.id,element).subscribe((project:project)=>{})
        
  //   //   });
      
  //   // })
  //   // this.projectservice.getProjectWithId(this.ID).subscribe((val:project)=>{
  //   //   val.accountIds.filter(num=>num!=accountId)
  //   //   this.getAccountsWithAccountIds(val.accountIds)
  //   // })


  //   this.projectservice.getProjectWithId(this.ID).subscribe((val:project)=>
  //   {
  //     val.accountIds = val.accountIds.filter(item=>item!=accountId);
  //     this.projectservice.updateProject(this.ID,val).subscribe()
  //     this.getAccountsWithAccountIds(val.accountIds)
  //   })
  // }

  getProjectWithId()
  {
    this.obser =  this.projectservice.getProjectWithId(this.ID);
    this.obser.subscribe((val:project) => {
      this.accountIds = val.accountIds;
     this.getAccountsWithAccountIds(val.accountIds);
    })
  }
  getAccountsWithAccountIds(accountIds : number[]){
    this.obser =  this.service.getAccounts(accountIds);
   
    this.obser.subscribe((val:accounts) => {
    this.list1=val;
   
  })
 }
navigateToAccountDetails(data: { id: string; }){
    this.route.navigate(['/forms/projects/' + this.ID + '/accounts/' + data.id]);
  }
}
