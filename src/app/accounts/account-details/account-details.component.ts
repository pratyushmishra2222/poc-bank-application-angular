import { Component,  OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { AccountServiceService } from 'src/app/account-service.service';
import { accounts } from 'src/app/accounts';


@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class AccountDetailsComponent implements OnInit {
 
  obser: Observable<accounts> | undefined;
  ID = '';
  name='';
  country='';
  
  constructor(private SpinnerService : NgxSpinnerService,private ActivatedRoute : ActivatedRoute,private service : AccountServiceService) { 
    this.ActivatedRoute.params.subscribe(data=>{
      
      this.ID = data.id1;
      this.send(this.ID);
      
    })
  }
  
  send(ID : any)
  {
    this.obser =  this.service.getAccountWithId(ID)
    this.SpinnerService.show();
    this.obser.subscribe((val:accounts) => {
     
      this.name = val.name
      this.country=val.country;
      this.SpinnerService.hide();
    })
  }
  
  ngOnInit(): void {}

}
