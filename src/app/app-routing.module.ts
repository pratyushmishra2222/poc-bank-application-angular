import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountDetailsComponent } from './accounts/account-details/account-details.component';
import { AuthGuard } from './auth.guard';
import { ErrorPageComponent } from './error-page/error-page.component';
import { FormsBasicComponent } from './forms-basic/forms-basic.component';
import { NewComponentComponent } from './new-component/new-component.component';
import { NotAllowedPageComponent } from './not-allowed-page/not-allowed-page.component';
import { ProjectDetailsComponent } from './projects/project-details/project-details.component';
import { ProjectsComponent } from './projects/projects.component';
import { SignupComponent } from './signup/signup.component';
import { TableComponentComponent } from './table-component/table-component.component';
import { UsersDetailsComponent } from './users/users-details/users-details.component';


const routes: Routes = [
  {path:'',redirectTo:'forms/login',pathMatch:'full'},
  {path:'not-allowed',component:NotAllowedPageComponent},
  {path:'forms/login',component:FormsBasicComponent , data: {state:  'forms'}},
  {path:'forms/signup',component:SignupComponent , data: {state:  'signup'}},
  

    {path:'forms/projects',component:ProjectsComponent,canActivate:[AuthGuard]},
    {
      path:'forms/projects',
    
    children:
    [
      {path:':id',component:ProjectDetailsComponent, data: {state:  'project'}},
      {path:':id/accounts/:id1',component:AccountDetailsComponent, data: {state:  'account'}},
      {path:':id/accounts/:id1/users/:id2',component:UsersDetailsComponent},
    ]
  },
   
 
  // {path:'table',component:TableComponentComponent},
  
  // {path:'**',component:ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
